package dev.pee.zopara;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

public class BurstEmitter extends Emitter {
	public BurstEmitter(Vector2 position, float radius, int ttl) {
		this.position = position;
		this.radius = radius;
		this.ttl = ttl;
		this.color = Color.FIREBRICK;
		particleCount = Integer.parseInt(Ui.particleNumberField.getText());
		particleSize = Ui.particleSizeSlider.getValue();
		PeeMain.emitters.add(this);
	}

	public void Create() {
		Random rnd = new Random();
		for (int i = 0; i < particleCount; i++) {
			PeeMain.particles.add(RandomizeParticle(rnd));
		}
	}

	private Particle RandomizeParticle(Random rnd) {
		Vector2 pDirection = new Vector2(rnd.nextFloat() * (rnd.nextBoolean() ? 1 : -1), rnd.nextFloat() * (rnd.nextBoolean() ? 1 : -1));
		pDirection.nor();
		// replace 10 with user input for speed
		pDirection.scl(rnd.nextFloat() * 10 * PeeMain.speedMultiplier);
		return new Particle(this.position, pDirection, (float) particleSize);
	}
}
