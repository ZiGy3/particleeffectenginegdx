package dev.pee.zopara;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PeeMain extends ApplicationAdapter {
	public static Stage stage;
	static ShapeRenderer renderer;
	AssetManager manager = new AssetManager();

	public static Array<String> emitterTypes = new Array<>(2);
	public static List<Particle> particles = new ArrayList();
	public static List<Emitter> emitters = new ArrayList();

	public static float speedMultiplier = 8;

	FPSLogger fpsLogger = new FPSLogger();

	@Override
	public void create () {
		manager.load("uiskin.json", Skin.class);
		manager.finishLoading();
		Skin skin = manager.get("uiskin.json", Skin.class);
		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);
		renderer = new ShapeRenderer();

		Ui.addUiElements(stage, skin);

		PeeInputProcessor inputProcessor = new PeeInputProcessor();
		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(stage);
		multiplexer.addProcessor(inputProcessor);
		Gdx.input.setInputProcessor(multiplexer);
	}

	@Override
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
		Ui.updatePosition(stage);
		renderer.setProjectionMatrix(stage.getCamera().combined);
	}

	@Override
	public void render () {
		fpsLogger.log();
		float dt = Gdx.graphics.getDeltaTime();
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		renderer.begin(ShapeRenderer.ShapeType.Filled);

		stage.act(dt);
		stage.draw();
		for (Emitter e:
				emitters) {
			renderer.setColor(e.color);
			renderer.circle(e.position.x, e.position.y, e.radius);
			if (e instanceof OverTimeEmitter && e.particleCount > 0)
				((OverTimeEmitter) e).EmitNext(dt);
			else e.ttl -= dt;
		}
		emitters.removeIf(x -> x.ttl < 1);

		renderer.setColor(Color.BLACK);
		Iterator<Particle> pIt = particles.iterator();
		while (pIt.hasNext()){
			Particle p = pIt.next();
			renderer.circle(p.x, p.y, p.radius);
			p.Update(dt, stage);
			if (p.CheckRemove())
				pIt.remove();
		}
		renderer.end();
	}
	
	@Override
	public void dispose () {
		renderer.dispose();
		stage.dispose();
	}
}
