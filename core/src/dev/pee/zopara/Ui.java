package dev.pee.zopara;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.List;

public class Ui {
	public static Label emitterTypeLabel;
	public static SelectBox emitterTypeSelect;
	public static Label particleNumberLabel;
	public static TextField particleNumberField;
	public static Label particleSizeLabel;
	public static Slider particleSizeSlider;
	public static Label particleDeltaLabel;
	public static Slider particleDeltaSlider;
	public static List<Actor> actors;

	public static void addUiElements(Stage stage, Skin skin) {
		actors = new ArrayList<>();

		// Emitter type label
		emitterTypeLabel = new Label("Emitter type:", skin);
		emitterTypeLabel.setHeight(32);
		emitterTypeLabel.setAlignment(Align.bottom);
		actors.add(emitterTypeLabel);

		// Emitter type select box
		PeeMain.emitterTypes.add("Burst", "Over-time");
		emitterTypeSelect = new SelectBox(skin);
		emitterTypeSelect.setItems(PeeMain.emitterTypes);
		emitterTypeSelect.setSelectedIndex(0);
		emitterTypeSelect.setSize(80, 32);
		actors.add(emitterTypeSelect);

		// Particle number label
		particleNumberLabel = new Label("Particle number:", skin);
		emitterTypeLabel.setHeight(32);
		emitterTypeLabel.setAlignment(Align.bottom);
		actors.add(particleNumberLabel);

		// Particle number field
		particleNumberField = new TextField("100", skin);
		particleNumberField.setTextFieldFilter(new TextField.TextFieldFilter.DigitsOnlyFilter());
		particleNumberField.setSize(64, 32);
		particleNumberField.setMaxLength(5);
		actors.add(particleNumberField);

		// Particle size label
		particleSizeLabel = new Label("Particle size:", skin);
		particleSizeLabel.setHeight(32);
		particleSizeLabel.setAlignment(Align.bottom);
		actors.add(particleSizeLabel);

		// Particle size slider
		particleSizeSlider = new Slider(1, 20, 1, false, skin);
		particleSizeSlider.setSize(64, 32);
		actors.add(particleSizeSlider);

		// Particle delta label
		particleDeltaLabel = new Label("Delta:", skin);
		particleDeltaLabel.setHeight(32);
		particleDeltaLabel.setAlignment(Align.bottom);
		actors.add(particleDeltaLabel);

		// Particle delta slider
		particleDeltaSlider = new Slider(1, 180, 1, false, skin);
		particleDeltaSlider.setSize(64, 32);
		actors.add(particleDeltaSlider);

		calculatePosition(actors, stage);
		for (Actor actor :
				actors) {
			stage.addActor(actor);
		}
	}

	public static void updatePosition(Stage stage) {
		calculatePosition(actors, stage);
	}

	private static void calculatePosition(List<Actor> actors, Stage stage) {
		if (actors.size() > 0) {
			actors.get(0).setPosition(16, stage.getHeight() - 32);
		}
		for (int i = 1; i < actors.size(); i++) {
			actors.get(i).setPosition(actors.get(i - 1).getX() + actors.get(i - 1).getWidth() + 16, stage.getHeight() - 32);
		}
	}
}
